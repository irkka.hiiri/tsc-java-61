package ru.tsc.ichaplygina.taskmanager.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

@Repository
@Scope("prototype")
public interface AbstractRepository<E extends AbstractModel> extends JpaRepository<E, String> {

}
