package ru.tsc.ichaplygina.taskmanager.service.model;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IProjectService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.User;
import ru.tsc.ichaplygina.taskmanager.repository.model.ProjectRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

@Service
@AllArgsConstructor
public final class ProjectService extends AbstractBusinessEntityService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void add(@NotNull final Project project) {
        repository.saveAndFlush(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final User user = Optional.ofNullable(userService.findById(userId)).orElseThrow(UserNotFoundException::new);
        @NotNull final Project project = new Project(name, description, user);
        add(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<Project> projectList) {
        if (projectList == null) return;
        for (final Project project : projectList) add(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(final String userId) {
        if (userService.isPrivilegedUser(userId)) repository.deleteAll();
        else repository.deleteByUserId(userId);
    }

    @Nullable
    @Override
    @Transactional
    public Project completeById(@NotNull final String userId, @Nullable final String projectId) {
        return updateStatus(userId, projectId, Status.COMPLETED);
    }

    @Nullable
    @Override
    @Transactional
    public Project completeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, repository.count())) throw new IndexIncorrectException(index);
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByPage(PageRequest.of(index, 1)).stream().findFirst().orElse(null) :
                repository.getIdByPageForUser(userId, PageRequest.of(index, 1)).stream().findFirst().orElse(null);
        return completeById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public Project completeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByName(name) :
                repository.getIdByNameForUser(userId, name);
        if (isEmptyString(id)) return null;
        return completeById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@NotNull final String userId) {
        return userService.isPrivilegedUser(userId) ?
                repository.findAll() : repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@NotNull final String userId, @Nullable final String sortBy) {
        @NotNull final Comparator<Project> comparator = getComparator(sortBy);
        return userService.isPrivilegedUser(userId) ?
                repository.findAll().stream().sorted(comparator).collect(Collectors.toList()) :
                repository.findAllByUserId(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findById(@NotNull final String userId, @Nullable final String projectId) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        return userService.isPrivilegedUser(userId) ?
                repository.findById(projectId).orElse(null) :
                repository.findFirstByUserIdAndId(userId, projectId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        return userService.isPrivilegedUser(userId) ?
                repository.findByIndex(PageRequest.of(entityIndex, 1)).stream().findFirst().orElse(null) :
                repository.findByIndexForUser(userId, PageRequest.of(entityIndex, 1)).stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        return userService.isPrivilegedUser(userId) ?
                repository.findFirstByName(entityName) :
                repository.findFirstByUserIdAndName(userId, entityName);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, final int index) {
        return userService.isPrivilegedUser(userId) ?
                repository.getIdByPage(PageRequest.of(index, 1)).stream().findFirst().orElse(null) :
                repository.getIdByPageForUser(userId, PageRequest.of(index, 1)).stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, @NotNull final String entityName) {
        return userService.isPrivilegedUser(userId) ?
                repository.getIdByName(entityName) : repository.getIdByNameForUser(userId, entityName);
    }

    @Override
    @SneakyThrows
    public long getSize() {
        return repository.count();
    }

    @Override
    @SneakyThrows
    public long getSize(@NotNull final String userId) {
        return userService.isPrivilegedUser(userId) ?
                repository.count() : repository.countByUserId(userId);
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final String userId) {
        return getSize(userId) == 0;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @Nullable final Project project = findById(id);
        if (project == null) return null;
        repository.deleteById(id);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeById(@NotNull final String userId, @NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @Nullable final Project project = findById(userId, id);
        if (project == null) return null;
        if (userService.isPrivilegedUser(userId)) repository.deleteById(id);
        else repository.deleteByUserIdAndId(userId, id);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        @Nullable Project project = findByIndex(userId, index);
        if (project == null) return null;
        if (userService.isPrivilegedUser(userId) || project.getUser().getId().equals(userId))
            repository.delete(project);
        else project = null;
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final Project project = findByName(userId, name);
        if (project == null) return null;
        if (userService.isPrivilegedUser(userId)) repository.deleteByName(name);
        else repository.deleteByUserIdAndName(userId, name);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project startById(@NotNull final String userId, @Nullable final String projectId) {
        return updateStatus(userId, projectId, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    @Transactional
    public Project startByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, repository.count())) throw new IndexIncorrectException(index);
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByPage(PageRequest.of(index, 1)).stream().findFirst().orElse(null) :
                repository.getIdByPageForUser(userId, PageRequest.of(index, 1)).stream().findFirst().orElse(null);
        return startById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public Project startByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByName(name) :
                repository.getIdByNameForUser(userId, name);
        return startById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project updateById(@NotNull final String userId,
                              @NotNull final String projectId,
                              @NotNull final String projectName,
                              @Nullable final String projectDescription) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        if (isEmptyString(projectName)) throw new NameEmptyException();
        @Nullable final Project project = findById(userId, projectId);
        if (project == null) return null;
        project.setName(projectName);
        project.setDescription(projectDescription);
        repository.saveAndFlush(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project updateByIndex(@NotNull final String userId,
                                 final int entityIndex,
                                 @NotNull final String entityName,
                                 @Nullable final String entityDescription) {
        if (isInvalidListIndex(entityIndex, getSize(userId))) throw new IndexIncorrectException(entityIndex + 1);
        @Nullable final String entityId = Optional.ofNullable(getId(userId, entityIndex)).orElse(null);
        if (entityId == null) return null;
        return updateById(userId, entityId, entityName, entityDescription);
    }

    @Nullable
    @Transactional
    private Project updateStatus(@NotNull final String userId, @Nullable final String projectId, @NotNull final Status status) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        @Nullable final Project project = findById(userId, projectId);
        if (project == null) return null;
        project.setStatus(status);
        repository.saveAndFlush(project);
        return project;
    }

}
