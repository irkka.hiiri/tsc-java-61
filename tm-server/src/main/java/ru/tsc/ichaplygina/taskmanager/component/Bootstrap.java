package ru.tsc.ichaplygina.taskmanager.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.api.service.IDomainService;
import ru.tsc.ichaplygina.taskmanager.api.service.ILogService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.*;
import ru.tsc.ichaplygina.taskmanager.api.service.model.*;
import ru.tsc.ichaplygina.taskmanager.endpoint.AbstractEndpoint;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Component
public final class Bootstrap {

    private static final boolean CONSOLE_LOG_ENABLED = false;

    @NotNull
    private static final String PID_FILE_NAME = "task-manager.pid";

   /* @NotNull
    private static final MessageExecutor messageExecutor = context.getBean(MessageExecutor.class);*/

    @NotNull
    @Autowired
    protected static ApplicationContext context;

    @NotNull
    @Autowired
    private ILogService logService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @NotNull
    @Autowired
    private IUserRecordService userRecordService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IProjectRecordService projectRecordService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskRecordService taskRecordService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectTaskRecordService projectTaskRecordService;

    @NotNull
    @Autowired
    private IProjectTaskService projectTaskService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private ISessionRecordService sessionRecordService;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private Backup backup;

    private void initData() {
        if (userService.findIdByLogin("root") == null)
            userService.add("root", "toor", "root@domain", Role.ADMIN, "Root", "Root", "Root");
        if (userService.findIdByLogin("user") == null)
            userService.add("user", "user", "user@user", Role.USER, "U.", "S.", "Er");
        @NotNull final String rootId = userService.findIdByLogin("root");
        @NotNull final String userId = userService.findIdByLogin("user");
        projectService.clear(rootId);
        projectService.clear(userId);
        projectService.add(rootId, "RootProject1", "");
        projectService.add(rootId, "RootProject2", "");
        projectService.add(userId, "UserProject1", "");
        projectService.add(userId, "UserProject2", "");
        taskService.add(rootId, "RootTask1", "");
        taskService.add(rootId, "RootTask2", "");
        taskService.add(userId, "UserTask1", "");
        taskService.add(userId, "UserTask2", "");
        taskService.addTaskToProject(rootId, taskService.getId(rootId, "RootTask1"), projectService.getId(rootId, "RootProject1"));
        taskService.addTaskToProject(rootId, taskService.getId(rootId, "RootTask2"), projectService.getId(rootId, "RootProject1"));
        taskService.addTaskToProject(rootId, taskService.getId(rootId, "UserTask1"), projectService.getId(rootId, "UserProject1"));
        taskService.addTaskToProject(rootId, taskService.getId(rootId, "UserTask2"), projectService.getId(rootId, "UserProject1"));
        userService.findById(rootId);
        userService.findById(rootId);
        userService.findById(rootId);
    }

    @SneakyThrows
    public void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String url = propertyService.getServer() + ":" + propertyService.getPort() + "/" +
                endpoint.getClass().getSimpleName() + "?wsdl";
        System.out.println(url);
        Endpoint.publish(url, endpoint);
    }

    @SneakyThrows
    public void initJMS() {
        BasicConfigurator.configure();
        @NotNull final BrokerService brokerService = new BrokerService();
        brokerService.addConnector("tcp://localhost:61616");
        brokerService.start();
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(PID_FILE_NAME), pid.getBytes());
        @NotNull final File file = new File(PID_FILE_NAME);
        file.deleteOnExit();
    }

    public void initRootUser() {
        try {
            userService.add("root", "toor", "root@domain", Role.ADMIN, "Root", "Root", "Root");
        } catch (@NotNull final AbstractException e) {
            logService.error(e);
        }
    }

    public void run(@NotNull final String... args) {
        initEndpoints();
        initPID();
        //    backup.init();
        initData();
        initJMS();
        if (userService.isEmpty()) {
            logService.info("No users loaded. Initializing default root user.");
            initRootUser();
        }
    }

 /*  public static void sendMessage(@NotNull final Object object, @NotNull final String type) {
        messageExecutor.sendMessage(object, type);
    }*/


}
