package ru.tsc.ichaplygina.taskmanager.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.api.service.ISenderService;
import ru.tsc.ichaplygina.taskmanager.dto.EntityLogDTO;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class MessageExecutor {

    private static final int THREAD_COUNT = 3;

    @NotNull
    private static MessageExecutor instance;

    @NotNull
    @Autowired
    private ISenderService service;

    @NotNull
    public static MessageExecutor getInstance() {
        return instance;
    }

    public MessageExecutor() {
        instance = this;
    }

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@NotNull final Object object, @NotNull final String type) {
        es.submit(() -> {
            @NotNull final EntityLogDTO entity = service.createMessage(object, type);
            service.send(entity);
        });
    }

    public void stop() {
        es.shutdown();
    }


}
