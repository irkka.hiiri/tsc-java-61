package ru.tsc.ichaplygina.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    long count();

    void deleteAll();

    void deleteById(@NotNull String id);

    void deleteByLogin(@NotNull String login);

    @NotNull
    List<User> findAll();

    @Nullable
    User findFirstByEmail(@NotNull String email);

    @NotNull
    Optional<User> findById(@NotNull String id);

    @Nullable
    User findFirstByLogin(@NotNull String login);

    @Nullable
    @Query("select id from User e where e.email = :email")
    String findFirstIdByEmail(@NotNull @Param("email") String email);

    @Nullable
    @Query("select id from User e where e.login = :login")
    String findFirstIdByLogin(@NotNull @Param("login") String login);

}
