package ru.tsc.ichaplygina.taskmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.entity.IWBS;
import ru.tsc.ichaplygina.taskmanager.listener.EntityListener;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_task")
@XmlAccessorType(XmlAccessType.FIELD)
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task extends AbstractBusinessEntity implements IWBS {

    @Nullable
    @ManyToOne
    @JsonIgnore
    private Project project;

    public Task(@NotNull final String name, @Nullable final String description, @NotNull final User user) {
        super(name, description, user);
    }

}
