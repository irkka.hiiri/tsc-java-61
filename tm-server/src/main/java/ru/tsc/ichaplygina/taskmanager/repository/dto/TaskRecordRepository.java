package ru.tsc.ichaplygina.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.dto.TaskDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskRecordRepository extends AbstractBusinessEntityRecordRepository<TaskDTO> {

    long count();

    long countByUserId(@NotNull String userId);

    void deleteAll();

    void deleteById(@NotNull String id);

    void deleteByName(@NotNull String name);

    void deleteByProjectId(@NotNull String projectId);

    void deleteByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    @Query("SELECT e FROM TaskDTO e")
    List<TaskDTO> findByIndex(@NotNull Pageable pageable);

    @NotNull
    @Query("SELECT e FROM TaskDTO e where e.userId = :userId")
    List<TaskDTO> findByIndexForUser(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull String userId);

    @NotNull
    Optional<TaskDTO> findById(@NotNull String id);

    @NotNull
    List<TaskDTO> findByProjectId(String projectId);

    @NotNull
    List<TaskDTO> findByUserIdAndProjectId(String userId, String projectId);

    @Nullable
    @Query("SELECT id FROM TaskDTO")
    List<String> getIdByPage(@NotNull Pageable pageable);

    @Nullable
    TaskDTO findFirstByIdAndProjectId(@NotNull String id, @NotNull String projectId);

    @Nullable
    TaskDTO findFirstByIdAndUserId(@NotNull String userId, @NotNull String id);

    @NotNull
    TaskDTO findFirstByName(@NotNull String name);

    @Nullable
    TaskDTO findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Nullable
    TaskDTO findFirstByUserIdAndIdAndProjectId(@NotNull String userId, @NotNull String id, @NotNull String projectId);

    @Nullable
    TaskDTO findFirstByUserIdAndName(@NotNull String userId, @NotNull String name);

    @NotNull
    @Query("SELECT id FROM TaskDTO e WHERE e.name = :name")
    String getIdByName(@NotNull @Param("name") String name);

    @NotNull
    @Query("SELECT id FROM TaskDTO e WHERE e.userId = :userId AND e.name = :name")
    String getIdByNameForUser(@NotNull @Param("userId") String userId, @Param("name") @NotNull String name);

    @Query("SELECT id FROM TaskDTO e where e.userId = :userId")
    List<String> getIdByPageForUser(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

}
