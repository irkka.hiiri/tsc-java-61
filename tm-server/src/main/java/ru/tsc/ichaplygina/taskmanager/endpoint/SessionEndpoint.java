package ru.tsc.ichaplygina.taskmanager.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.ichaplygina.taskmanager.api.service.model.ISessionService;
import ru.tsc.ichaplygina.taskmanager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(name = "SessionEndpoint")
public class SessionEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @WebMethod
    public void closeSession(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.closeSession(session);
    }

    @WebMethod
    public Session openSession(@WebParam(name = "login") @NotNull final String login,
                               @WebParam(name = "password") @NotNull final String password) {
        return sessionService.openSession(login, password);
    }

}
