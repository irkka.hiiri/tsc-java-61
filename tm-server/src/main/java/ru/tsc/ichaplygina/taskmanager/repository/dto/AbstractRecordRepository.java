package ru.tsc.ichaplygina.taskmanager.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.dto.AbstractModelDTO;

@Repository
@Scope("prototype")
public interface AbstractRecordRepository<E extends AbstractModelDTO> extends JpaRepository<E, String> {

}
