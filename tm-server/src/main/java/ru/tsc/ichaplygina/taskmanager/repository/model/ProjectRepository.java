package ru.tsc.ichaplygina.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface ProjectRepository extends AbstractBusinessEntityRepository<Project> {

    long count();

    long countByUserId(@NotNull String userId);

    void deleteAll();

    void deleteById(@NotNull String id);

    void deleteByName(@NotNull String name);

    void deleteByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    @NotNull
    Optional<Project> findById(@NotNull String id);

    @NotNull
    @Query("SELECT e FROM Project e")
    List<Project> findByIndex(@NotNull Pageable pageable);

    @NotNull
    @Query("SELECT e FROM Project e where e.user.id = :userId")
    List<Project> findByIndexForUser(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

    @Nullable
    Project findFirstByIdAndUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findFirstByName(@NotNull String name);

    @Nullable
    Project findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findFirstByUserIdAndName(@NotNull String userId, @NotNull String name);

    @Nullable
    @Query("SELECT id FROM Project e WHERE e.name = :name")
    String getIdByName(@NotNull @Param("name") String name);

    @Nullable
    @Query("SELECT id FROM Project e WHERE e.user.id = :userId AND e.name = :name")
    String getIdByNameForUser(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    @NotNull
    @Query("SELECT id FROM Project")
    List<String> getIdByPage(@NotNull Pageable pageable);

    @Query("SELECT id FROM Project e where e.user.id = :userId")
    List<String> getIdByPageForUser(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

}
