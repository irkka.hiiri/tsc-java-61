package ru.tsc.ichaplygina.taskmanager.api.property;

import org.jetbrains.annotations.NotNull;

public interface ISessionProperty {

    @NotNull Integer getSignIteration();

    @NotNull String getSignSecret();

}
