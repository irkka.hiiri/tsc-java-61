package ru.tsc.ichaplygina.taskmanager.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

@Repository
@Scope("prototype")
public interface AbstractBusinessEntityRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> {

}
