package ru.tsc.ichaplygina.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskRepository extends AbstractBusinessEntityRepository<Task> {

    long count();

    @Query("SELECT COUNT(e) FROM Task e WHERE e.user.id = :userId")
    long countByUserId(@NotNull @Param("userId") String userId);

    void deleteAll();

    void deleteById(@NotNull String id);

    void deleteByName(@NotNull String name);

    void deleteByProjectId(@NotNull String projectId);

    void deleteByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    Optional<Task> findById(@NotNull String id);

    @NotNull
    @Query("SELECT e FROM Task e")
    List<Task> findByIndex(@NotNull Pageable pageable);

    @NotNull
    @Query("SELECT e FROM Task e where e.user.id = :userId")
    List<Task> findByIndexForUser(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

    @NotNull
    List<Task> findByProjectId(String projectId);

    @NotNull
    List<Task> findByUserIdAndProjectId(String userId, String projectId);

    @Nullable
    Task findFirstByIdAndProjectId(@NotNull String id, @NotNull String projectId);

    @Nullable
    Task findFirstByIdAndUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findFirstByName(@NotNull String name);

    @Nullable
    Task findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findFirstByUserIdAndIdAndProjectId(@NotNull String userId, @NotNull String id, @NotNull String projectId);

    @Nullable
    Task findFirstByUserIdAndName(@NotNull String userId, @NotNull String name);

    @Nullable
    @Query("SELECT id FROM Task e WHERE e.name = :name")
    String getIdByName(@NotNull @Param("name") String name);

    @Nullable
    @Query("SELECT id FROM Task e WHERE e.user.id = :userId AND e.name = :name")
    String getIdByNameForUser(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    @NotNull
    @Query("SELECT id FROM Task")
    List<String> getIdByPage(@NotNull Pageable pageable);

    @NotNull
    @Query("SELECT id FROM Task e where e.user.id = :userId")
    List<String> getIdByPageForUser(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);


}
