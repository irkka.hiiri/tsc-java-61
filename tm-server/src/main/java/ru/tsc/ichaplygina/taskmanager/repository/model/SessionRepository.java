package ru.tsc.ichaplygina.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.Session;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractRepository<Session> {

    long count();

    void deleteAll();

    void deleteById(@NotNull final String id);

    @NotNull
    List<Session> findAll();

    @NotNull
    Optional<Session> findById(@NotNull final String id);

}
