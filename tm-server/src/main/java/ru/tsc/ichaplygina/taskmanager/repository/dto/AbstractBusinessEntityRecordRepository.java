package ru.tsc.ichaplygina.taskmanager.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.dto.AbstractBusinessEntityDTO;

@Repository
@Scope("prototype")
public interface AbstractBusinessEntityRecordRepository<E extends AbstractBusinessEntityDTO> extends AbstractRecordRepository<E> {

}
