package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.dto.EntityLogDTO;

public interface ILoggerService {

    void writeLog(EntityLogDTO message);

}
