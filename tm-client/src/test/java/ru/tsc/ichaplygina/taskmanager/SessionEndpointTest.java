package ru.tsc.ichaplygina.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.endpoint.Session;
import ru.tsc.ichaplygina.taskmanager.endpoint.SessionEndpoint;
import ru.tsc.ichaplygina.taskmanager.endpoint.SessionEndpointService;
import ru.tsc.ichaplygina.taskmanager.marker.SoapCategory;

import javax.xml.ws.WebServiceException;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @Test
    @Category(SoapCategory.class)
    public void testOpenSession() {
        Assert.assertNotNull(sessionEndpoint.openSession("root", "toor"));
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testOpenSessionFail() {
        sessionEndpoint.openSession("cat", "meow");
    }

    @Test
    @Category(SoapCategory.class)
    public void testCloseSession() {
        @NotNull final Session session = sessionEndpoint.openSession("root", "toor");
        sessionEndpoint.closeSession(session);
    }

}
