package ru.tsc.ichaplygina.taskmanager.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;
import ru.tsc.ichaplygina.taskmanager.listener.AbstractListener;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

@Component
public final class AboutListener extends AbstractListener {

    @NotNull
    public final static String ARG_NAME = "-a";
    @NotNull
    public final static String CMD_NAME = "about";
    @NotNull
    public final static String DESCRIPTION = "show developer info";

    @NotNull
    @Override
    public final String argument() {
        return ARG_NAME;
    }

    @NotNull
    @Override
    public final String command() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@aboutListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        printLinesWithEmptyLine(Manifests.read("developer"), Manifests.read("email"));
    }

}
