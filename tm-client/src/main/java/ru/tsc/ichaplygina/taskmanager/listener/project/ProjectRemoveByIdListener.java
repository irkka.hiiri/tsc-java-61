package ru.tsc.ichaplygina.taskmanager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class ProjectRemoveByIdListener extends AbstractProjectListener {

    @NotNull
    public final static String DESCRIPTION = "remove project by id";
    @NotNull
    public final static String NAME = "remove project by id";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final String id = readLine(ID_INPUT);
        getProjectEndpoint().removeProjectById(sessionService.getSession(), id);
    }

}
