package ru.tsc.ichaplygina.taskmanager.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;
import ru.tsc.ichaplygina.taskmanager.listener.AbstractListener;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

@Component
public class VersionListener extends AbstractListener {

    @NotNull
    public final static String ARG_NAME = "-v";
    @NotNull
    public final static String CMD_NAME = "version";
    @NotNull
    public final static String DESCRIPTION = "show version info";

    @NotNull
    @Override
    public final String argument() {
        return ARG_NAME;
    }

    @NotNull
    @Override
    public final String command() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@versionListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        printLinesWithEmptyLine(Manifests.read("build"));
    }

}
