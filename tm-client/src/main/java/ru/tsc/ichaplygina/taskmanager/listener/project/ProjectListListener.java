package ru.tsc.ichaplygina.taskmanager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.endpoint.Project;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.DELIMITER;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.SORT_HINT;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @NotNull
    public final static String DESCRIPTION = "show all projects";
    @NotNull
    public final static String NAME = "list projects";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectListListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final String sortBy = readLine(SORT_HINT);
        @Nullable final List<Project> projectList = getProjectEndpoint().getProjectList(sessionService.getSession(), sortBy);
        if (projectList == null) return;
        System.out.println("Id : Name : Description : Created : Status : Start Date : End Date : User Id");
        int index = 1;
        for (@NotNull final Project project : projectList) {
            System.out.println(index + DELIMITER +
                    project.getId() + DELIMITER +
                    project.getName() + DELIMITER +
                    project.getDescription() + DELIMITER +
                    project.getCreated() + DELIMITER +
                    project.getStatus() + DELIMITER +
                    project.getDateStart() + DELIMITER +
                    project.getDateFinish() + DELIMITER +
                    project.getUser().getId());
            index++;
        }
    }

}
