package ru.tsc.ichaplygina.taskmanager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

@Component
public final class ProjectCompleteByIndexListener extends AbstractProjectListener {

    @NotNull
    public final static String DESCRIPTION = "complete project by index";
    @NotNull
    public final static String NAME = "complete project by index";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectCompleteByIndexListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        final int index = readNumber(INDEX_INPUT);
        getProjectEndpoint().completeProjectByIndex(sessionService.getSession(), index);
    }

}
