package ru.tsc.ichaplygina.taskmanager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class ProjectCompleteByNameListener extends AbstractProjectListener {

    @NotNull
    public final static String DESCRIPTION = "complete project by name";
    @NotNull
    public final static String NAME = "complete project by name";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectCompleteByNameListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final String name = readLine(NAME_INPUT);
        getProjectEndpoint().completeProjectByName(sessionService.getSession(), name);
    }

}
