package ru.tsc.ichaplygina.taskmanager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.endpoint.Task;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.DELIMITER;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.SORT_HINT;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public class TaskListListener extends AbstractTaskListener {

    @NotNull
    public final static String DESCRIPTION = "show all tasks";
    @NotNull
    public final static String NAME = "list tasks";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskListListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final String sortBy = readLine(SORT_HINT);
        @NotNull final List<Task> taskList = getTaskEndpoint().getTaskList(sessionService.getSession(), sortBy);
        if (taskList == null) return;
        System.out.println("Id : Name : Description : Created : Status : Start Date : End Date : User Id");
        int index = 1;
        for (@NotNull final Task task : taskList) {
            System.out.println(index + DELIMITER +
                    task.getId() + DELIMITER +
                    task.getName() + DELIMITER +
                    task.getDescription() + DELIMITER +
                    task.getCreated() + DELIMITER +
                    task.getStatus() + DELIMITER +
                    task.getDateStart() + DELIMITER +
                    task.getDateFinish() + DELIMITER +
                    task.getUser().getId());
            index++;
        }
    }

}
