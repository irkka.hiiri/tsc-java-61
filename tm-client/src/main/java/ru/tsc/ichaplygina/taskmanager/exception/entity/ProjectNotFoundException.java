package ru.tsc.ichaplygina.taskmanager.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class ProjectNotFoundException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Project not found.";

    public ProjectNotFoundException() {
        super(MESSAGE);
    }

}
